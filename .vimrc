let mapleader = ','
" /usr/share/vim/vimcurrent/debian.vim) and sourced by the call to :runtime
" you can find below.  If you wish to change any of those settings, you should
" do it in this file (/etc/vim/vimrc), since debian.vim will be overwritten
" everytime an upgrade of the vim packages is performed.  It is recommended to
" make changes after sourcing debian.vim since it alters the value of the
" 'compatible' option.

" This line should not be removed as it ensures that various options are
" properly set to work with the Vim-related packages available in Debian.
runtime! debian.vim

" Uncomment the next line to make Vim more Vi-compatible
" NOTE: debian.vim sets 'nocompatible'.  Setting 'compatible' changes numerous
" options, so any other options should be set AFTER setting 'compatible'.
"set compatible

" Vim5 and later versions support syntax highlighting. Uncommenting the next
" line enables syntax highlighting by default.
if has("syntax")
  syntax on
endif

" Uncomment the following to have Vim jump to the last position when
" reopening a file
if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif

" The following are commented out as they cause vim to behave a lot
" differently from regular Vi. They are highly recommended though.
"set showcmd		" Show (partial) command in status line.
set showmatch		" Show matching brackets.
set ignorecase		" Do case insensitive matching
set smartcase
set incsearch
set hlsearch
nnoremap <silent> <CR> :noh<CR><CR>
"set autowrite		" Automatically save before commands like :next and :make
"set hidden             " Hide buffers when they are abandoned
"set mouse=a		" Enable mouse usage (all modes)

" Source a global configuration file if available
"if filereadable("/etc/vim/vimrc.local")
"  source /etc/vim/vimrc.local
"endif

" Mine!!
" inoremap "<tab> ""<left>
" inoremap [<tab> []<left>
" inoremap (<tab> ()<left>
" inoremap {<tab> {}<left>
" inoremap '<tab> ''<left>
inoremap kj <esc>
inoremap jk <esc>
inoremap kk <esc>
inoremap jj <esc>


" Text 'Objects'
onoremap <silent> ib :<C-U>normal! f(vi(<cr>
onoremap <silent> ab :<C-U>normal! f(va(<cr>
nnoremap <silent> df ^dt{da{
nnoremap <silent> ds %my%r<space>`yr<space>
nnoremap <silent> c, ct,
nnoremap <silent> d, dt,
" Split line, and edit in the middle
nnoremap E i<cr><cr><up>

" dont move on *
nnoremap * *<c-o>
nnoremap n nzz
nnoremap N Nzz
nnoremap G Gzz
"set scrolloff=1000

" edit .vimrc
nnoremap <leader>rc :tabedit $MYVIMRC<cr>

set number

" I dont rack a disciprine
"inoremap <esc> <nop>
" Kill the Movement keys
noremap <UP> <C-W><C-W>
noremap <DOWN> <C-W><C-W>
noremap <RIGHT> :tabnext<CR>
noremap <LEFT> :tabprevious<CR>
" inoremap <silent> <UP>    <esc><c-w><c-w>
" inoremap <silent> <DOWN>  <esc><c-w><c-w>
" inoremap <silent> <RIGHT> <esc>:tabnext<cr>
" inoremap <silent> <LEFT>  <esc>:tabprevious<cr>

nnoremap H ^
nnoremap L $

noremap <INSERT> :wqa<CR>
inoremap <INSERT> <ESC>:wqa<CR>
noremap <F10> :!clear && date && acpi && sensors<CR>
inoremap <F10> <ESC>:! clear && date && acpi && sensors<CR>

"Move Lines of text up and down"
nnoremap <C-j> ddp
nnoremap <C-k> ddkP
nnoremap <C-l> xp
nnoremap <C-h> xhP

" Dumb way of getting 'pseudo Multiple cursors'"
inoremap <C-t> <ESC>`a.``
noremap <C-t> `a.``
inoremap <C-g> <ESC>`a.`b.`c.`d.`e.``
noremap <C-g> `a.`b.`c.`d.`e.``

noremap <S-Tab> :tabedit

" FileType considerations
autocmd BufRead,BufNewFile   *.cpp,*.cc,*.c,*.h,*.java,*.cs,*.js nnoremap
\<buffer> <leader>c I// <esc>j

\nnoremap <buffer> <leader>u ^t/3xk
autocmd BufRead,BufNewFile   *.lua nnoremap <buffer> <leader>u I<esc>l3xk
\ | nnoremap <buffer> <leader>c I-- <esc>j
\ | nnoremap <buffer> <leader><leader>m :wa<cr>:!love . &<cr>


" Colors ??
set t_Co=256
hi Comment	ctermfg=darkgray ctermbg=NONE
"hi Normal	ctermfg=253 ctermbg=234
hi String	ctermfg=magenta cterm=bold
hi Type	ctermfg=65
hi Statement	ctermfg=70 cterm=bold
hi Constant	ctermfg=208
hi Search	ctermbg=darkblue
hi matchparen ctermfg=white ctermbg=3

" Timeout Length
set timeoutlen=580

" Show troublesome whitespace
set list
set listchars+=tab:→.,trail:.,extends:\#,nbsp:.,eol:↲
" → u+2192
" ↲ u+21b2
" ⏎ u+????
hi nontext ctermfg=234
hi specialkey ctermfg=234
nnoremap <F3> :set list!<cr>
inoremap <F3> <esc>:set list!<cr>gi

" Formatting
set autoindent
set tabstop=4
" For python indentation
"autocmd BufRead,BufNewFile *.py :setlocal expandtab smarttab shiftwidth=2 tabstop=2

" Movement
nnoremap <tab> %

" tags
nnoremap <leader>[ <C-w><C-]>
 " Go to declaration or definition and open in new Tab
nnoremap <leader>] <C-w><C-]><C-w>T

" surround fake
 nnoremap <leader>1 viwdi"<esc>pa"<esc>h
 nnoremap <leader>2 viwdi'<esc>pa'<esc>h
 nnoremap <leader>3 viwdi(<esc>pa)<esc>h
 nnoremap <leader>4 viwdi[<esc>pa]<esc>h
 nnoremap <leader>5 viwdi{<esc>pa}<esc>h
 nnoremap <leader>6 viwdi<<esc>pa><esc>h
 nnoremap <leader>7 viwdi/<esc>pa/<esc>h
 nnoremap <leader>0 viwdp<esc>bXXe

" Change inside easier
nnoremap c1 ci"
nnoremap c2 ci'
nnoremap c3 ci(
nnoremap c4 ci[
nnoremap c5 ci{
nnoremap c0 f(ci(
" Append inside ...
nnoremap <leader>c1 di"Pa
nnoremap <leader>c2 di'Pa
nnoremap <leader>c3 di(Pa
nnoremap <leader>c4 di[Pa
nnoremap <leader>c5 di{Pa
nnoremap <leader>c0 ^f(di(Pa

" Insert blank line without going to "hell" mode
nnoremap \ o<esc>k$
nnoremap \|  O<esc>j$

" Get Rid of stupid whitespace
" Gets rid of lines with just whitespace
" And trailing white space
nnoremap <Leader>f<space> :%s/[\t ]\+$//g<cr>
"nnoremap <Leader>f<space> :/^[\t ]\+$<cr>

" Toglle some formatting options
let g:MyFormattingOptsFlag = 0
let g:MyTextWidthForPar = 75
function! MyFormattingOptsToggle()
 if g:MyFormattingOptsFlag
  set formatprg=
  vnoremap = =
  set textwidth=0
  set formatoptions=croql
  let g:MyFormattingOptsFlag = 0
 else
  execute 'set formatprg=par\ -rejw'.g:MyTextWidthForPar
  vnoremap = gq
  set textwidth=80
  set formatoptions=croqlt
  let g:MyFormattingOptsFlag = 1
 endif
endfunction
nnoremap <leader>tf :call MyFormattingOptsToggle()<cr>

"Copy selection to clipboard"
"Assumes the program 'xsel' is installed"
vnoremap <C-c> :w !xsel -b<cr><cr>

"Copy selection to a 'pastebin' paste and paste URL into clipboard"
" Assumes xsel is installed. Also perl.
vnoremap <leader>pb :w !perl -e '
	\while(<>){$data .= $_ ;}
	\print "api_option=paste&
	\api_dev_key=9bcc4fd765e0f85875558ff6eb89bc5e&
	\api_paste_private=0&
	\api_paste_code=$data";'
	\ \| POST pastebin.com/api/api_post.php
	\ \| xsel -b <cr><cr>

"Mail Selection ? "
" Assumess openssl, base64 are installed
function! MyIntegratedEmailClient()
	call inputsave()
	"let g:p = ""
	let g:p= input("Password: ")
	let g:t = ""
"	while g:t !=? "\n"
"		let g:t = getchar()
"		let g:p += g:t
"	endwhile
	"echom g:p

	let g:rcpt = input("RCPT: ")
	let g:from = input("From: ")
	let g:cmd = ''

	call inputrestore()
endfunction
nnoremap <leader>em :call MyIntegratedEmailClient()<cr>

""openssl s_client -connect smtp.gmail.com:25 -starttls smtp
" EHLO
" AUTH LOGIN, responde en base64 Usename: mandar el nombre completo del email
" Responde en base64 Password: mandar el password en base64
" :P
" COn auth plain se debe mandar el resultado de este comando
" echo -e "\0allfre2luzon@gmail.com\0xxxxxxxxxxxxxxx | base64 | xsel -b
" Las x son el password

"Get the definition of the word under the cursor from 'rae'"
"Assumes 'perl' is installed and 'html2text'"
nnoremap <leader>D :set encoding=latin1<cr>
		\yiwo<esc>pbviw
		\:!`perl -e 'print "GET http://lema.rae.es/drae/srv/search?val=" . <>;'` \|
		\html2text \| head -n 9 <cr><cr>
		\:set encoding=utf-8<cr>

" ??
nnoremap <Space> A
nnoremap S i<cr><esc>
" ??

" Caracteres especiales del espaniol
inoremap `a    á
inoremap `A    Á
inoremap `e    é
inoremap `E    É
inoremap `i    í
inoremap `I    Í
inoremap `o    ó
inoremap `O    Ó
inoremap `u    ú
inoremap `U    Ú

" Explore filesystem from within vim
nnoremap <leader>O :vsp .<cr>:vertical resize 20<cr>
let IsExploreTheFilesystemWithNetrwOn = 0
let ExploreTheFilesystemWithNetrwPath = ""
function! ExploreTheFilesystemWithNetrw()
	if g:IsExploreTheFilesystemWithNetrwOn
		let g:IsExploreTheFilesystemWithNetrwOn = 0
		bdelete "" + g:ExploreTheFilesystemWithNetrwPath
	else
		let g:IsExploreTheFilesystemWithNetrwOn = 1
		let g:ExploreTheFilesystemWithNetrwPath = expand('%f')
		vsp .
		vertical resize 20
	endif
endfunction
nnoremap <silent> <F4> :call ExploreTheFilesystemWithNetrw()<cr>

" Set backup directories so vim doesn't write files on working directory
set dir=~/tmp,/var/tmp,/tmp
set backupdir=~/tmp,~/

" Correction plugin be me ???
nnoremap <leader>Z :set spell spelllang=es<cr>
nnoremap <leader>z [se
nnoremap <leader>c 1z=
 " inoremap <space> <space><esc>[s1z=gia

" keep good humor all the time
command WQ wq
command Wq wq
command W w
command Q q
nnoremap ; :
let g:MyToggleCenterScreenWhenMoveIsEnabled = 1
if MyToggleCenterScreenWhenMoveIsEnabled
nnoremap j gjzz
nnoremap k gkzz
else
nnoremap j gj
nnoremap k gk
endif

function! MyToggleCenterScreenWhenMove()
	if g:MyToggleCenterScreenWhenMoveIsEnabled
		nnoremap j gj
		nnoremap k gk
		let g:MyToggleCenterScreenWhenMoveIsEnabled = 0
	else
		nnoremap j gjzz
		nnoremap k gkzz
		let g:MyToggleCenterScreenWhenMoveIsEnabled = 1
	endif
endfunction
nnoremap <f6> :call MyToggleCenterScreenWhenMove()<cr>

" separate words
nnoremap <leader><space> i<space><esc>we

filetype plugin on
" Plugins that i use:
" Snipmate: https://github.com/msanders/snipmate.vim
" Multiple Cursors: https://github.com/terryma/vim-multiple-cursors
" Keep cursors after esc in insert mode
let g:multi_cursor_exit_from_insert_mode = 0
function! MultipleCursorOnEveryMatchByMe()
	normal! yiw
	let tmp = @"
	exec ":MultipleCursorsFind " . tmp
endfunction
nnoremap <leader><insert> :call MultipleCursorOnEveryMatchByMe()<cr>
nnoremap <leader><delete> :MultipleCursorsFind<space>

" highlight characters past the 80th column
highlight OverLength ctermbg=gray ctermfg=white
match OverLength /\%80v.\+/
" Highlight current line
set cursorline
highlight CursorLine cterm=none ctermbg=234

" Get my public ip using mysite
nnoremap <leader>ip o<esc>!!GET alfredoluzon.com/euler/env.pl \| grep -i remote_addr \| sed -e 's/[^0-9.]//g'<cr><cr>

" Source easy
vnoremap <silent> <leader>so y:exec @"<cr>
nnoremap <leader>so :w<cr>:so%<cr>


" Complete filenames and complete lines in insert mode
inoremap <c-l> <c-x><c-l>
inoremap <c-f> <c-x><c-f>

" Send file or selection to a private gist and leave de link in the clipboard
" assumess that "xsel" and "perl" are installed
nnoremap <leader>pg :call SendGist()<cr>
function! SendGist()

	let g:myfiletype = &filetype
	let g:name = expand('%:t')
	let g:perlCode = ':w ! perl -e
		\ ''while(<>){$data .= $_;}
		\ $data =~ s/\\/\\\\/g;
		\ $data =~ s/"/\\"/g;
		\ $data =~ s/\n/\\n/g;
		\ $data =~ s/\t/\\t/g;
		\ print "{\"public\":false,
		\  \"files\":{\"' . g:name . '\":
		\  {\"content\":\"".$data . " \"}}}";''
		\ | POST -c "application/json" https://api.github.com/gists
		\ | grep -io "html_url\":\"[^,]\+,"
		\ | grep -io "htt[^\"]\+" | xsel -b'
	exec g:perlCode
	echom "Done"
endfunction

""""""""" temp

" Edit files in my site directly
function! AlfredoLuzonDotComEdit(path)
	let g:SiteUrl = 'allfre2@alfredoluzon.com/'
	let  g:netrw_ftpextracmd = "passive"
	let  g:netrw_ftp_cmd = "ftp -p"
	echo a:path
	call NetUserPass($upuser,$uppass)
	exec ":tabedit ftp://" . g:SiteUrl . a:path
endfunction
nnoremap <leader>re :call AlfredoLuzonDotComEdit("/")<left><left>

" Status line ?
set laststatus=2
set statusline=\"%f\"\ %=%b\ 0x%B\ [%v,%l]/%L
" Show diference beetwen insert and normal mode
autocmd InsertEnter * hi CursorLine cterm=none ctermbg=none
autocmd InsertLeave * hi CursorLine cterm=none ctermbg=233

" Split behaveour
set splitbelow
"set splitright

" Practice
" Input

function! ASDF()
	call inputsave()
	let g:x = input("RCPT: ")
	clear
	echom g:x
	call inputrestore()
endfunction

" Remember what you have to do
let RememberWhatIHaveToDoIsOpen = 0
function! RememberWhatIHaveToDo()

	let todoPath = '$HOME/.todo'
	if g:RememberWhatIHaveToDoIsOpen
		let g:RememberWhatIHaveToDoIsOpen = 0
		bdelete .todo
	else
		let g:RememberWhatIHaveToDoIsOpen = 1
		exe 'sp' .todoPath
		resize 5
	endif
endfunction
nnoremap <silent> <F1> :call RememberWhatIHaveToDo()<cr>

" Toggle "hex editor" mode
nnoremap <leader>hx :%!xxd<cr>


" Switch the working direnctory to the the directory of the file in
" the current buffer
nnoremap <Leader>cd :cd %:p:h<cr>:pwd<cr>

" Clear undo history
function! MyClearUndoHistory()
	let old_undolevels = &undolevels
	set undolevels=-1
	exe "normal a \<BS>\<Esc>"
	let &undolevels = old_undolevels
	unlet old_undolevels
endfunction

" Calculate with octave
function! MyPassToOctaveHack()
	let g:input = @"
	exec 'echo ' . shellescape(g:input) . ' | octave  --silent | cut -c8-'
endfunction


vnoremap <leader>= :!octave --silent \| cut -c8-<cr>

" Toggle mouse in all modes
let g:MyMouseIsEnabledFlag = 0
function! MyToggleMouseEnabledInAllModes()

if g:MyMouseIsEnabledFlag
	let g:MyMouseIsEnabledFlag = 0
	set mouse=""
 else
	let g:MyMouseIsEnabledFlag = 1
	set mouse=a
endif
endfunction
nnoremap <silent> <leader>M :call MyToggleMouseEnabledInAllModes()<cr>

" Capitalize words
nnoremap <leader>~ viw~

nnoremap <F5> :silent update<Bar>silent !chromium %:p &<CR>

" Edit Live using python simpleHTTPserver and live.js
let g:MyLiveJsDownloadURL = "http://livejs.com/live.js"
let g:MyLiveEditBrowser = "iceweasel"
function! MyLiveEditCreateSession()
" TODO: Complete mess but dont have time
" Creates a index.html in the current file with links to all html files
" on the working directory recursively.
" If it doesn't see a live.js file in the directory it downloads it with
" wget from the url configured here.

" This assumes that the index file is created, and live.js is included in all
" html files that you wish lo live edit
	 exe '!python -m SimpleHTTPServer 8000 > /dev/null 2>&1 &'
	 exe '!' . g:MyLiveEditBrowser . ' 0.0.0.0:8000 &'
	 " WARNING: Have to kill python server process manually after =(
endfunction
autocmd BufRead,BufNewFile   *.html,*.htm nnoremap <leader>SS :call MyLiveEditCreateSession()<cr>

" Easy typing of some things
let g:MyEasyTypeSourroundedStuffFlag = 0
function! MyEasyTypeSourroundedStuff()
	if g:MyEasyTypeSourroundedStuffFlag
	  inoremap " "
	  inoremap ' '
	  inoremap ( (
	  inoremap { {
	  inoremap [ [
	  let g:MyEasyTypeSourroundedStuffFlag = 0
	else
	  inoremap " ""<left>
	  inoremap ' ''<left>
	  inoremap ( ()<left>
	  inoremap { {}<left>
	  inoremap [ []<left>
	  let g:MyEasyTypeSourroundedStuffFlag = 1
	endif

endfunction
nnoremap <silent> <leader>@ :call MyEasyTypeSourroundedStuff()<cr>

" Formatting visual selection
" Columns
vnoremap <leader>fc !column -t<cr>

" Scroll windows up and down
nnoremap <c-up> <c-y>
nnoremap <c-down> <c-e>

" Fold method
set foldmethod=syntax
set foldlevelstart=99
highlight folded  ctermbg=none ctermfg=236

" Consejos para recordar
" En insert-mode
" 	ctrl-Y insertar el caracter arriba del cursor
" 	ctrl-E insertar el caracter debajo del cursor


" Useful ??

" Breaks if:
" Leave while editing in between pairs of matching characters
" type nested pairs of characters
function! EasyOutOfSourroundings(setTabOut)
if a:setTabOut
	inoremap <tab> <right><right><c-o>:call EasyOutOfSourroundings(0)<cr>
	inoremap <cr> <right><right><c-o>:call EasyOutOfSourroundings(0)<cr>
else
	inoremap <tab> <tab>
	inoremap <cr> <cr>
endif
endfunction

set matchpairs+=\":\",':',/:/
inoremap <C-]> (){}<left><cr><cr><up>
inoremap <silent> {<cr> {}<left><cr><cr><up>
inoremap <silent> () ()<left><c-o>:call EasyOutOfSourroundings(1)<cr>
inoremap <silent> [] []<left><c-o>:call EasyOutOfSourroundings(1)<cr>
inoremap <silent> "" ""<left><c-o>:call EasyOutOfSourroundings(1)<cr>
inoremap <silent> '' ''<left><c-o>:call EasyOutOfSourroundings(1)<cr>
inoremap <silent> {} {}<left><c-o>:call EasyOutOfSourroundings(1)<cr>
"abbreviate ){ ){}

" Delete every character on the line but keeps the line
nnoremap <silent> dL :s/.//g<cr>:noh<cr>

" Download a file to a vim buffer
" Assumes that the file url is pasted in the clipboard
nnoremap <silent> <leader>DE :.!GET `xsel -b`<cr>

" Upper case word
inoremap <c-u> <esc>gUiwea
nnoremap <c-u> viwUe
